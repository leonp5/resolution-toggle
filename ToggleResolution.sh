#!/bin/bash

# Works only with XFCE Desktop/Tools

### Variables ###

WQHD="2560x1440"
UHD="3840x2160"
configFile="resolutionswitch.conf"
absolutePathToScript="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
pathToIcons="$absolutePathToScript/icons/"

### Declaring functions ###

function checkConfigFileExistence()
{
	[ -e $configFile ]
}

function createDefaultConfigFile()
{
	defaultSettings="mode=WQHD \n# not used at the moment \nWQHD="$WQHD" \n# not used at the moment \nUHD="$UHD" \nwindowStyle=mx-comfort \nDPI=96"
	echo -e "$defaultSettings" > $configFile
}

# make the key value pairs of the conf file globaly available
function setConfig()
{	
	# Makes all variables inside the config file available inside the script.
	# After! setConfig run, for getting a value simply write $key in the script.
	# Example: mode=day inside the resolutionswitch.conf. Inside the script: $mode returns day
	source $configFile
}

function setResolution()
{
	xrandr --output HDMI-3 --mode "$1"
}

function setDpi()
{
	xfconf-query -c xsettings -p /Xft/DPI -s $1
	writeToConfFile "DPI" "$1"
}

# $1 = key, $2 = value
function writeToConfFile()
{
	sed -i "s/^\("$1"\s*=\s*\).*$/\1$2/" $configFile
}

function setWindowStyle()
{
	xfconf-query -c xfwm4 -p /general/theme -s $1
	writeToConfFile "windowStyle" "$1"

}

# $1 = panel height, $2 = icon size
function setPanelSize()
{
	xfconf-query --create --channel 'xfce4-panel' --property '/panels/panel-1/size' --type 'int' --set $1

	xfconf-query --create --channel 'xfce4-panel' --property '/panels/panel-1/icon-size' --type 'int' --set $2
}


# $1 = message
function showSuccessNotification()
{
	# expanding the title with tabs, increase the notification width
	notify-send -i ""$pathToIcons"resolution.png" "ResolutionSwitch			" "$1"
}

# $1 = message
function showErrorNotification()
{
	# expanding the title with tabs, increase the notification width
	notify-send -i ""$pathToIcons"dialog-error.svg" "ResolutionSwitch			" "$1"
}

function reloadXfcePanel()
{
	bash -c 'xfce4-panel -r'
}

function toggleResolution()
{
	if [ $mode = "WQHD" ]; then
		dpi=139
		panelSize=50
		iconSize=32
	    # setResolution "$UHD"
		setPanelSize "$panelSize" "$iconSize"
		setDpi "$dpi"
		writeToConfFile "mode" "UHD"
		setWindowStyle "nanodesu-large"
		showSuccessNotification "UHD mode active.\nDPI: "$dpi". \nPanel: "$panelSize"px.\nIcons: "$iconSize"px."
		reloadXfcePanel
		exit
	else
		# setResolution "$WQHD"
		dpi=96
		panelSize=24
		iconSize=15
		setPanelSize "$panelSize" "$iconSize"
		setDpi "$dpi"
		writeToConfFile "mode" "WQHD"
		setWindowStyle "mx-comfort"
		showSuccessNotification "(WQ)HD mode active.\nDPI: "$dpi". \nPanel: "$panelSize"px.\nIcons: "$iconSize"px."
		reloadXfcePanel
		exit
	fi

	showErrorNotification "Error occured!"
}

### Executing functions

checkConfigFileExistence
configExists=$?

if [[ "$configExists" != 0 ]]; then
	createDefaultConfigFile
fi

setConfig

toggleResolution